//package be.tekeasy.intelligence.entities;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//
//@Entity
//public class Company_copy {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private long id;
//    private final String name;
//    private final String bvdIdNumber;
//
//    public Company_copy() {
//        this.name = "";
//        this.bvdIdNumber = "";
//    }
//
//    public Company_copy(String name, String bvdIdNumber) {
//        this.name = name;
//        this.bvdIdNumber = bvdIdNumber;
//    }
//
//    public long getId() {
//        return id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getBvdIdNumber() {
//        return bvdIdNumber;
//    }
//
//    @Override
//    public String toString() {
//        return "Company{" + "id=" + id + ", name=" + name + ", BvD ID number=" + bvdIdNumber + '}';
//    }
//}
