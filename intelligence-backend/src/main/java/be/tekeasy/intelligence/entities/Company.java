package be.tekeasy.intelligence.entities;

import javax.persistence.*;

@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "bvdidnumber")
    private Integer bvdidnumber;


    public Company() {

    }

    public Company(String name, Integer bvdidnumber) {
        this.name = name;
        this.bvdidnumber = bvdidnumber;
    }

    public Company(String name, Integer bvdidnumber, boolean b) {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBvdidnumber() {
        return bvdidnumber;
    }

    public void setBvdidnumber(Integer bvdidnumber) {
        this.bvdidnumber = bvdidnumber;
    }


    @Override
    public String toString() {
        return "Company [id=" + id + ", name=" + name + ", bvdidnumber=" + bvdidnumber + "]";
    }
}
