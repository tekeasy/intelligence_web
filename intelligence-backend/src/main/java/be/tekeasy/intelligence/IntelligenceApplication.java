package be.tekeasy.intelligence;

import be.tekeasy.intelligence.entities.Company;
import be.tekeasy.intelligence.repositories.CompanyRepository;
import be.tekeasy.intelligence.repositories.UsersRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class IntelligenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntelligenceApplication.class, args);
	}

//	@Bean
//	CommandLineRunner init(CompanyRepository companyRepository) {
//		return args -> {
//			Stream.of("UCB", "Tekeasy", "Delhaize", "Capgemini", "Bvd").forEach(name -> {
//				Company company = new Company(name, name.toLowerCase() + " TVA Number");
//				companyRepository.save(company);
//			});
//			companyRepository.findAll().forEach(System.out::println);
//
//
//		};
//	}
}
