//package be.tekeasy.intelligence.controllers;
//
//import be.tekeasy.intelligence.entities.Company;
//import be.tekeasy.intelligence.repositories.CompanyRepository;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@CrossOrigin(origins = "http://localhost:4200")
//public class CompanyController_copy {
//
//    private final CompanyRepository companyRepository;
//
//    public CompanyController_copy(CompanyRepository companyRepository) {
//        this.companyRepository = companyRepository;
//    }
//
//    @GetMapping("/companies/copy")
//    public List<Company> getUsers() {
//        return (List<Company>) companyRepository.findAll();
//    }
//
//    @PostMapping("/companies/copy")
//    void addUser(@RequestBody Company company) {
//        companyRepository.save(company);
//    }
//}