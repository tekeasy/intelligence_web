package be.tekeasy.intelligence.controllers;

import be.tekeasy.intelligence.entities.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import be.tekeasy.intelligence.repositories.UsersRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")



public class UsersController {
    @Autowired
    UsersRepository usersRepository;

    @GetMapping("/users")
    public ResponseEntity<List<Users>> getAllUsers(@RequestParam(required = false) String firstname) {
        try {
            List<Users> users = new ArrayList<Users>();

            if (firstname== null)
                usersRepository.findAll().forEach(users::add);
            else
                usersRepository.findByFirstname(firstname).forEach(users::add);

            if (users.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Users> getUsersById(@PathVariable("id") long id) {
        Optional<Users> usersData = usersRepository.findById(id);

        if (usersData.isPresent()) {
            return new ResponseEntity<>(usersData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/users")

    public ResponseEntity<Users> createUsers(@RequestBody Users users) {
        try {
            Users _users = usersRepository
                    .save(new Users(users.getFirstname(), users.getEmail(), users.getPassword()));
            return new ResponseEntity<>(_users, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @PutMapping("/users/{id}")
//    public ResponseEntity<Users> updateUsers(
//            @PathVariable("id") long id,
//            @Valid @RequestBody Users users) {
//        Optional<Users> usersData = usersRepository.findById(id);
//
//        if (usersData.isPresent()) {
//            Users _users = usersData.get();
//            _users.setFirstname(_users.getFirstname());
//            _users.setEmail(_users.getEmail());
//            _users.setPassword(_users.getPassword());
//            return new ResponseEntity<>(usersRepository.save(_users), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//
//   }

    @PutMapping("/users/{id}")

    public ResponseEntity<Users> updateUser(
            @PathVariable(value = "id") Long id,
            @Valid @RequestBody Users userDetails)  {
        Users user = usersRepository.findById(id)
                .orElseThrow();
        user.setFirstname(userDetails.getFirstname());
        user.setEmail(userDetails.getEmail());
        final Users updatedUser = usersRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }


    @DeleteMapping("/users/{id}")
    public ResponseEntity<HttpStatus> deleteUsers(@PathVariable("id") long id) {
        try {
            usersRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/users")
    public ResponseEntity<HttpStatus> deleteAllUsers() {
        try {
            usersRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



}
