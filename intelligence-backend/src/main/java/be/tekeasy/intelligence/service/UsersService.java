//package be.tekeasy.intelligence.service;
//
//import java.util.ArrayList;
//import java.util.List;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import be.tekeasy.intelligence.entities.Users;
//import be.tekeasy.intelligence.repositories.UsersRepository;
//
//public class UsersService {
//
//    @Autowired
//    UsersRepository usersRepository;
//    //getting all books record by using the method findaAll() of CrudRepository
//    public List<Users> getAllUsers()
//    {
//        List<Users> users = new ArrayList<Users>();
//        usersRepository.findAll().forEach(users1 -> users.add(users1));
//        return users;
//    }
//    //getting a specific record by using the method findById() of CrudRepository
//    public Users getUsersById(long id)
//    {
//        return usersRepository.findById(id).get();
//    }
//    //saving a specific record by using the method save() of CrudRepository
//    public  void saveOrUpdate(Users users)
//    {
//        usersRepository.save(users);
//    }
//    //deleting a specific record by using the method deleteById() of CrudRepository
//    public void delete(long id)
//    {
//        usersRepository.deleteById(id);
//    }
//    //updating a record
//    public void update(Users users, long id)
//    {
//        usersRepository.save(users);
//    }
//}
//
