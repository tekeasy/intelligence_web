package be.tekeasy.intelligence.repositories;

import java.util.List;;

import be.tekeasy.intelligence.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UsersRepository extends JpaRepository<Users, Long> {
    List<Users> findByEmail(String email);

    List<Users> findByFirstname(String firstname);
}


