package be.tekeasy.intelligence.repositories;

import java.util.List;

import be.tekeasy.intelligence.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CompanyRepository extends JpaRepository<Company, Long> {
    List<Company> findByName(String email);

    List<Company> findByBvdidnumber(Integer bvdidnumber);
}
