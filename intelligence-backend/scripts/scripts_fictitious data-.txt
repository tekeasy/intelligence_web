-------------------Insert fictitious data------------------

--Table company--

INSERT INTO public.company (
name, bvdidnumber) 
VALUES 
(
'UCB'::character varying, '123'::integer
),
(
'Tekeasy'::character varying, '456'::integer
),
(
'Delhaize'::character varying, '789'::integer
),
(
'Capgemini'::character varying, '153'::integer
),
(
'Bvd'::character varying, '135'::integer
);


--Table users--


INSERT INTO public.users (
firstname, email, password) VALUES 
('toto'::character varying, 'toto@toto.com'::character varying, 'toto'::character varying),
('titi'::character varying, 'titi@titi.com'::character varying, 'titi'::character varying)