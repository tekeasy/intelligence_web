package be.tekeasy.intelligence.config;

import be.tekeasy.intelligence.service.CompanyService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class CompanyConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("be.tekeasy.intelligence.wsdl");
        return marshaller;
    }

    @Bean
    public CompanyService countryClient(Jaxb2Marshaller marshaller) {
        CompanyService client = new CompanyService();
        client.setDefaultUri("http://localhost:8080/ws");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}
