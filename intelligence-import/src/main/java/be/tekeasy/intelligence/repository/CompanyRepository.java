package be.tekeasy.intelligence.repository;

import be.tekeasy.intelligence.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
