package be.tekeasy.intelligence;


import be.tekeasy.intelligence.service.CompanyService;
import be.tekeasy.intelligence.wsdl.GetCompanyDetailsResponse;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class IntelligenceImportApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntelligenceImportApplication.class, args);
    }

    @Bean
    CommandLineRunner lookup(CompanyService companyService) {
        return args -> {
            String country = "tekeasy";
            if (args.length > 0) {
                country = args[0];
            }
            GetCompanyDetailsResponse response = companyService.getCompanyDetails(country);
            System.err.println(response.getCompany().getEmail());
        };
    }
}
