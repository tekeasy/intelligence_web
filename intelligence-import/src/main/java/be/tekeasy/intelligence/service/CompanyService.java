package be.tekeasy.intelligence.service;

import be.tekeasy.intelligence.wsdl.GetCompanyDetailsRequest;
import be.tekeasy.intelligence.wsdl.GetCompanyDetailsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class CompanyService extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(CompanyService.class);

    public GetCompanyDetailsResponse getCompanyDetails(String name) {

        GetCompanyDetailsRequest request = new GetCompanyDetailsRequest();
        request.setName(name);

        log.info("Requesting company details for company id: " + name);

        GetCompanyDetailsResponse response = (GetCompanyDetailsResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/companies", request,
                        new SoapActionCallback(
                                "https:/tekeasy.be/CompanyService/GetCompanyRequest"));
        return response;
    }

}