package be.tekeasy.intelligence.controller;

import be.tekeasy.intelligence.model.Company;
import be.tekeasy.intelligence.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public class CompanyController {

    @Autowired
    CompanyRepository companyRepository;

    public ResponseEntity<Company> createCompany(@RequestBody Company company) {
        try {
            Company _company = companyRepository
                    .save(new Company(company.getName(), company.getEmail()));
            return new ResponseEntity<>(_company, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
