import { Injectable } from '@angular/core';
import{HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';;
import { catchError, map, flatMap } from 'rxjs/operators';
import { User } from '../users/users-model/users.model';


@Injectable({
  providedIn: 'root'
})
export class ServerService {
  private BASE_URL='http://localhost:8080/api/';

  constructor(private http: HttpClient) { }

   public get<T>(url: string): Observable<any>
   {return this.http.get(this.BASE_URL + url);}

   public post<T>(url:string, body:T, secure: boolean = true): Observable<any>
    {
      return this.http.post(this.BASE_URL + url, body);
    }
  




  public put<T>(url:string, body:T): Observable<any>
  {

    return this.http.put(this.BASE_URL + url, body);
    
  }

  public delete<T>(url:string): Observable<any>
  {

    return this.http.delete(this.BASE_URL + url);
    
  }


   

    public login(user: User): Observable <boolean>
   {
     return this.http.post<any>(this.BASE_URL + 'users', user).pipe(
       map((data: any) => {
         if(data.success)
         {
          
           user.id = data.id;
           sessionStorage.setItem('user', JSON.stringify(user));

           console.log("TOKEN_RENEWED");
           return true;
         }
         return false;
       }), catchError((res: any) =>
       {
         return of(false);
       })
       
       );
   } 
 
   public logout(): void
   {
     sessionStorage.removeItem('user');
    // sessionStorage.removeItem('id_token');
   }
 

}
