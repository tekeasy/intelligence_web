export class User
{
  
  id: number;
  firstname: string;
  email: string;
  password: string;

  constructor (data: any)
  {
    this.id = data.id;
    this.firstname = data.firstname;
    this.email = data.email;
    this.password = data.password;
  }
}
