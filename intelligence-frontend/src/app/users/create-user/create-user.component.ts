import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../users-model/users.model';
import { AuthService } from '../users_auth/auth-service/auth.service';
import { UsersService } from '../users-service/users.service';
import { MatRadioModule } from "@angular/material/radio";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  userForm!: FormGroup;
  firstnameCtl!: FormControl;
  emailCtl!: FormControl;
  isNew: boolean = true;
  user!:User;
  passwordCtl!: FormControl;
  passwordConfirmCtl!: FormControl;


  constructor(private userService: UsersService,private router: Router, 
    private route: ActivatedRoute, 
    private formBuilder: FormBuilder,
    private authService: AuthService ) {this.initForm(); }

  ngOnInit(): void {
    if(this.route.snapshot.params["id"])
    {
      this.userService.getOneById(this.route.snapshot.params["id"]).subscribe(m => 
        {
          if(m)
          {   
            this.isNew = false;
            this.user = m;
            this.userForm.patchValue(this.user);
          }
        });
    }
  }

  initForm(): void
{
  this.emailCtl = this.formBuilder.control ('', [Validators.required], [this.emailExist()]); 

    this.passwordCtl = this.formBuilder.control('', [Validators.required, Validators.minLength(6), this.checkPassword()]);
    this.passwordConfirmCtl = this.formBuilder.control('', [Validators.required, this.checkConfirm()]);
    this.firstnameCtl = this.formBuilder.control ('', [Validators.required, Validators.minLength(1)]);

    this.userForm = this.formBuilder.group({
      firstname: this.firstnameCtl,
      email: this.emailCtl,
      password: this.passwordCtl,
      passwordConfirm: this.passwordConfirmCtl


});
}

emailExist(): any
{
  var timeout: any;
  return (ctl: FormControl) =>
  {
    clearTimeout(timeout);
    const email = ctl.value;
    return new Promise(resolve => {
      timeout = setTimeout(() =>{
        if(ctl.pristine)
        {
          resolve(null);
        } 
      }, 300)
    });
  }
}


checkPassword(): ValidatorFn
{
return (control: AbstractControl): {[key: string]: any} | null => {
  const password = control.value;
  
  let hasNumber = /\d/;
  let hasErrors = false;
  let errors = {forbidden: {value: 'passwords and user name are equal'}, pwdAndUsernameEqual: false, pwdMustContainNbr: false, 
    pwdRegEx: false };
  var pattern = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$"
  );

  if(password === this.emailCtl.value && (!control.hasError('required')))
  {
    hasErrors = true;
    errors.pwdAndUsernameEqual = true;
  } else if(!hasNumber.test(password))
  {
    hasErrors = true;
    errors.pwdMustContainNbr = true; 
  } else if(!pattern.test(password))
  {
    hasErrors = true;
    errors.pwdRegEx = true;
  }
  return hasErrors ? errors : null;
};
}



checkConfirm(): ValidatorFn
{
return (control: AbstractControl): {[key: string]: any} | null => {
  const password = control.value;

  if(password != this.passwordCtl.value && (!control.hasError('required')))
  {
    return  {forbidden: {value: 'passwords are not the same'}, pwdNotEqual: true };
  }
  return null;
};
}

onSubmit()
  {
    const formVal = this.userForm.value;
    formVal.id = 0;
    const newUser = new User(formVal);
   
     this.authService.login (newUser).subscribe(m=>
       {
        //this.router.navigate(['/users/'+this.authService.getCurrentUser().id]);// on se logue automatiquement après la création d'un user
        this.router.navigate(['/users/'])
       }); 
   }


}

