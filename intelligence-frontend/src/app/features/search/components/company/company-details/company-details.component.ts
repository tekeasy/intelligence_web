import { Component, OnInit } from '@angular/core';
import {CompanyDetailsModel} from '../../../models/company/company-details/company-details.model';
import{CompanyDetailsService} from '../company-details-service/company-details-service.service';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {

   companies: CompanyDetailsModel[] = [  ];

  constructor(private companyDetailsService: CompanyDetailsService) {
  }

  ngOnInit() {
    this.companyDetailsService.findAll().subscribe(data => {
      this.companies = data;
    });
  }
}
