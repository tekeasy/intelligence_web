import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Company.DetailsComponent } from './company.details.component';

describe('Company.DetailsComponent', () => {
  let component: Company.DetailsComponent;
  let fixture: ComponentFixture<Company.DetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Company.DetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Company.DetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
