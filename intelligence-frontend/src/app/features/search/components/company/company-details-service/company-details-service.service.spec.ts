import { TestBed } from '@angular/core/testing';

import { CompanyDetailsServiceService } from './company-details-service.service';

describe('CompanyDetailsServiceService', () => {
  let service: CompanyDetailsServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompanyDetailsServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
