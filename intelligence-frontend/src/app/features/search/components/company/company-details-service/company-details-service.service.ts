import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CompanyDetailsModel } from '../../../models/company/company-details/company-details.model';
import { Observable } from 'rxjs';

@Injectable()
export class CompanyDetailsService {

  private companiesUrl: string;

  constructor(private http: HttpClient) {
    this.companiesUrl = 'http://localhost:8080/companies';
  }

  public findAll(): Observable<CompanyDetailsModel[]> {
    return this.http.get<CompanyDetailsModel[]>(this.companiesUrl);
  }


}
