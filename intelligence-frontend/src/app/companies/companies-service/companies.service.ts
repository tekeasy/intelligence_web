import { Injectable } from '@angular/core';
import{ServerService} from '../../server/server.service'
import {Companies} from '../../companies/companies-model/companies.model';
import { Observable } from 'rxjs';
import{ map, catchError } from 'rxjs/operators';
import{Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  constructor(private server: ServerService, private router : Router) { }

  public getAll(): Observable<Companies[]>
{
  return this.server.get<Companies[]>('companies').pipe(
    map(res => res.map((m: any) =>new Companies(m))),
    catchError(err=>
     {
       console.error(err);
       return[];
     } )
  );
}
}
