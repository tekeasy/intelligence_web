import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import {Companies} from '../companies-model/companies.model';
import { CompaniesService} from '../companies-service/companies.service';





@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'bvdidnumber'];
  dataSource!: MatTableDataSource<Companies>;
  public companiesList!: Companies[];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private companiesService: CompaniesService,
              private router: Router,
              public dialog: MatDialog,
              public snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.refresh();
  }

  refresh()
  {
    this.companiesService.getAll().subscribe(companies =>
    {
      this.companiesList = companies;
      this.updateDataSource();
    });
  }

  updateDataSource()
  {
    this.dataSource = new MatTableDataSource(this.companiesList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


}
