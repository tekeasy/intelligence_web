import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import{ CompanyDetailsComponent } from './features/search/components/company/company-details/company-details.component';
import{UsersComponent} from '../app/users/users-component/users.component'
import { CompaniesComponent } from './companies/companies-component/companies.component';
import { CreateUserComponent } from './users/create-user/create-user.component';
import { AuthComponent } from './users/users_auth/auth-component/auth.component';

const routes: Routes = [
{path: 'companies', component:CompaniesComponent},
{path : 'users', component:UsersComponent},
{path : 'auth', component: AuthComponent},
{path: 'create-user', component: CreateUserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
